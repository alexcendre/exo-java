import org.junit.Test;

public class TestCompterMots {
    @Test
    public void compteVide() {
        org.junit.Assert.assertEquals((int)new CompterMots().compterMots(""),0);
    }
    @Test
    public void compteUnMot() {
        org.junit.Assert.assertEquals((int)new CompterMots().compterMots("test"),1);
    }
    @Test
    public void compte4Mot() {
        org.junit.Assert.assertEquals((int)new CompterMots().compterMots("ceci est un test"),4);
    }
    @Test
    public void compte4MotAvecBlancAvAp() {
        org.junit.Assert.assertEquals((int)new CompterMots().compterMots("   ceci est un test   "),4);
    }
    @Test
    public void compte4MotAvecRcEtBlancAvAp() {
        org.junit.Assert.assertEquals((int)new CompterMots().compterMots("   ceci\nest\tun test \n  "),4);
    }
}
